'use strict'
const moment = require('moment')
const crypto = require('crypto')
const config = require('./config')


function createError(error, message) {
  return {
    message: message || 'Something was wrong',
    error: error || 'default error'
  }
}


function encryptValue(dato) {
  const encriptador = crypto.createHash('sha256')
  encriptador.update(dato)
  return encriptador.digest('hex')
}

function generateId() {
  return crypto.randomBytes(45).toString('hex')
}

function generateToken() {
  return crypto.randomBytes(10).toString('hex')
}

module.exports={
  encryptValue,
  generateId,
  generateToken,
  createError
}