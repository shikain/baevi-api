'use strict'
const utilities = require('../utils')
const pool = require('./pool').pool
const errors = require('../constants/errors')
const Message = require('../models/Message')
const Issue = require('../models/Issue')

function create({issuer}) {
  return new Promise((resolve, reject) => {
    let id = utilities.generateId()
    pool.query(`insert into issues (publicId,insertedBy,modifiedBy,statusId) select ?, users.id ,users.id , 1 from users where publicId=? `
      , [id, issuer], (err, status) => {
        if (err) return reject(utilities.createError(err.sqlMessage, `Hubo un problema creando la conversación`))
        if (!status.insertId) return reject(utilities.createError(errors.INVALID_PARAMS, `Hubo un problema creando la conversación`))
        return resolve(id)
      })
  })
}

function postMessage({messagerId, issueId, text}) {
  return new Promise((resolve, reject) => {
    pool.query(`insert into messages (publicId,insertedBy,modifiedBy,text,issueId) select ?,u.id,u.id,?,i.id from users u
   left join issues i on 1=1 where u.publicId=? and i.publicId=?`
      , [utilities.generateId(), text, messagerId, issueId], (err, status) => {
        if (err) return reject(utilities.createError(err.sqlMessage, `Hubo un problema creando el mensaje`))
        if (!status.insertId) return  reject(utilities.createError(errors.INVALID_PARAMS, `Hubo un problema creando el mensaje.`))
        return resolve(status.insertId)
      })
  })
}

function listMessages({issueId, offset, limit}) {
  return new Promise((resolve, reject) => {
    let values = [issueId]
    let query = `select m.publicId,m.insertedDate,m.modifiedDate,u.publicId as insertedBy,m.text,i.publicId issueId 
    from messages m left join issues i on i.id=m.issueId 
    left join users u on u.id = m.insertedBy
    where i.publicId=? and i.isActive=1 and m.isActive=1 order by
    insertedDate  asc `
    if(offset && limit){
      query += ` limit ?,?`
      values.push(offset)
      values.push(limit)
    }
    pool.query(query, values, (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, `Hubo un problema obteniendo los mensajes`))
      return  resolve(rows.map(m => {
        return new Message(m)
      }))

    })
  })
}
function getIssue({issueId, userId}) {
  let queryAux=``
 if(issueId) queryAux=`and i.publicId="${issueId}"`
  if(userId) queryAux +=`and u.publicId="${userId}" and (s.id = 1 or s.id = 2)`
  return new Promise((resolve, reject) => {
    pool.query(`select i.publicId,i.insertedDate,i.modifiedDate,s.value status,u.publicId issuerId,u.username issuerUsername, u.fullName issuerName,t.value issuerRole,
     u2.publicId solverId,u2.username solverUsername,u2.fullName solverName,s.value from issues i left join
     users u on i.insertedBy=u.id left join
     types t on t.id=u.typeId left join
     users u2 on i.solverId=u2.id left join 
     status s on s.id=i.statusId
     where i.isActive=1 ${queryAux} order by insertedDate ASC `, (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, `Hubo un problema obteniendo los datos de la conversación`))
      return resolve(rows.map(i => {
        return new Issue(i)
      }))

    })
  })
}
function takeIssue({issueId, userId}) {
  return new Promise((resolve, reject) => {
    console.log(userId)
    pool.query(`update issues set solverId=(select id from users where publicId=?),statusId=2,modifiedBy=(select id from users where publicId=?) where publicId=? and isActive=1 `, [userId,userId,issueId],(err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, `Error tomando la conversacion`))
      if(rows.affectedRows===0) return reject(utilities.createError(errors.INVALID_PARAMS, `Error tomando la conversacion, verifique los parametros`))
      return resolve()
    })
  })
}
function finishIssue({issueId, userId}) {
  return new Promise((resolve, reject) => {
    pool.query(`update issues set modifiedBy=(select id from users where publicId=?),statusId=3 where publicId=? and isActive=1 `, [userId,issueId],(err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, `Error eliminando la conversacion`))
      if(rows.affectedRows===0) return reject(utilities.createError(errors.INVALID_PARAMS, `Error eliminando la conversacion, verifique los datos`))
      return resolve()
    })
  })
}

module.exports = {
  create,
  postMessage,
  listMessages,
  getIssue,
  takeIssue,
  finishIssue
}