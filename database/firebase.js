'use strict'
const utils = require('../utils')
const pool = require('./pool').pool

function create({userPublicId, tokenId}) {
  return new Promise((resolve, reject) => {
    const consulta = `
    insert into users_firebaseId (userId, firebaseId)
    select id, ? from users where publicId=?
    on duplicate key update userId=values(userId);`
    pool.query(consulta, [tokenId, userPublicId], (error,status) => {

      if (error) return reject(utils.createError(error.sqlMessage, "There was a problem inserting your token, try again."))
      resolve()
    })
  })
}

function list({userIds = [], typesOfUser}) {
  return new Promise((resolve, reject) => {
    let consulta = `select firebaseId from users_firebaseId as firebase `
    let values = []
    if (userIds.length > 0) consulta += `left join users as user on user.id=firebase.userId where user.publicId in ("${userIds.join('","')}")`
    else if (typesOfUser) consulta += ` left join users as user on user.id=firebase.userId where user.typeID in(${typesOfUser.map(type=>`'${type}'`).join(',')})`
    consulta += ';'
    let a = pool.query(consulta, values, (error, rows) => {
      console.log(a.sql)
      if (error) return reject(utils.createError(error.sqlMessage, "There was a problem getting the firebases id, try again."))
      resolve(rows.map(row => row.firebaseId))
    })
  })
}

module.exports = {
  create,
  list
}
