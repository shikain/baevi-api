'use strict'
const utilities = require('../utils')
const pool = require('./pool').pool
const errors = require('../constants/errors')
const Promition=require('../models/Promotion')
function list() {
  return new Promise(((resolve, reject) => {
    pool.query(`Select publicId, insertedDate,modifiedDate,name,logo,description,image1,image2,image3,image4,facebook,instagram,twitter,site,address,latitude,longitude,phone,textAddress
    from promotions where isActive=1 `,[],(err,rows)=>{
      if(err) return reject(utilities.createError(err.sqlMessage,`Error obteniendo las promociones`))
      return  resolve(rows.map(r=> new Promition(r)))
    })
  }))
}function listFixers() {
  return new Promise(((resolve, reject) => {
    pool.query(`Select publicId, insertedDate,modifiedDate,name,logo,description,image1,image2,image3,image4,facebook,instagram,twitter,site,address,latitude,longitude,phone,textAddress
    from promotions where isActive=1 and tipo=2`,[],(err,rows)=>{
      if(err) return reject(utilities.createError(err.sqlMessage,`Error obteniendo las promociones`))
      return  resolve(rows.map(r=> new Promition(r)))
    })
  }))
}

module.exports = {
  list,
  listFixers
}