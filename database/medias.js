'use strict'
const utilities = require('../utils')
const pool = require('./pool').pool
const errors = require('../constants/errors')
const Media = require('../models/Media')
const config = require('../config')

function storeFile({media, userId}) {
  return new Promise(((resolve, reject) => {
    pool.query(`insert into medias (publicId,insertedBy,modifiedBy,url,typeId) select ?,u.id,u.id,?,3 from users u where 
  u.publicId=?`, [utilities.generateId(), `${media.url}`, userId], (err, status) => {
      if (err) return reject(utilities.createError(err.sqlMessage, 'Hubo un problema insertando el archivo en la base.'))
      if (!status.insertId) return reject(utilities.createError(errors.INVALID_PARAMS, 'Hubo un problema insertando el archivo en la base.'))
      return resolve(status.insertId)

    })
  }))
}

function updatePlaylist({url, userId}) {
  return new Promise(((resolve, reject) => {
    pool.query(`Update medias set url=? ,modifiedDate=Now(),modifiedBy=(select id from users where publicId=?) where typeId=2`, [url, userId], (err, status) => {
      if (err) return reject(utilities.createError(err.sqlMessage, 'Hubo un problema actualizando la playlist'))
      return resolve()

    })
  }))
}

function getPlaylist() {
  return new Promise(((resolve, reject) => {
    pool.query(`Select url from medias where typeId=2`, (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, 'Hubo un problema trayendo la playlist'))
      if (!rows) return reject(utilities.createError(err.sqlMessage, 'No existe playlist'))
      return resolve(rows[0])

    })
  }))
}

function getMedia(list = [], type = "") {
  return new Promise(((resolve, reject) => {
    let aux = ``
    let auxtype = ``
    if (list.length) aux = `and m.publicId in ("${list.join('","')}")`
    if (type !== "") auxtype`and m.typeId="${type}"`
    let a = pool.query(`select m.publicId, m.modifiedDate,m.insertedDate,m.url, t.value type from medias m
        left join types t on t.id=m.typeId where m.isActive=1 ${auxtype} ${aux} `, (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "No se pudo traer la multimedia"))
      return resolve(rows.map(r => new Media(r)))
    })
  }))
}

function postMedia({userId, type, url}) {
  return new Promise(((resolve, reject) => {
    pool.query(`INSERT into medias(publicId,insertedBy,modifiedBy,url,typeId)
     select ?,u.id,u.id,?,t.id from users u left join types t on 1=1 where u.isActive=1 
     and u.publicId=? and t.value=?`, [utilities.generateId(), url, userId, type], (err, status) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "Error insertando la multimedia"))
      if (!status.insertId) return reject(utilities.createError(errors.INVALID_PARAMS, "Error insertando la multimedia"))
      return resolve()
    })
  }))
}

function deleteMedia({userId, mediaId}) {
  return new Promise(((resolve, reject) => {
    pool.query(`select id from users u where publicId=? and u.isActive=1`, [userId], (err, user) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "Error obteniendo el usuario que elimina"))
      if (!user.length) return reject(utilities.createError(errors.INVALID_PARAMS, "No existe usuario con permisos para eliminar"))
      let idUser = user[0].id

      pool.query(`update medias set isActive=0,modifiedDate=NOW(), modifiedBy=? where publicId=?`, [idUser,mediaId], (err, status) => {
        if (err) return reject(utilities.createError(err.sqlMessage, "Error eliminando la multimedia"))
        if (status.affectedRows===0) return reject(utilities.createError(errors.INVALID_PARAMS, "Error eliminando la multimedia"))
        return resolve()
      })
    })
  }))
}


module.exports = {
  storeFile,
  updatePlaylist,
  getPlaylist,
  getMedia,
  postMedia,
  deleteMedia
}