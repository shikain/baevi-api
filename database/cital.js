'use strict'
const utilities = require('../utils')
const pool = require('./pool').pool
const errors = require('../constants/errors')
const mercadopago = require("mercadopago");
// Agrega credenciales
mercadopago.configure({
    access_token: "TEST-cb70b997-9347-4b71-9ab3-53fcab0d1010",
});

function getAsientos(idArr) {
    let where=``
    if(idArr) {
         where+=`and id in (`
        where+=idArr.join(',')
        where+=`)`
    }
    return new Promise((resolve, reject) => {
        let a=pool.query(`select * from asientosCital where isActive=1 ${where}`
            , (err, status) => {
                console.log(a.sql)
                if (err) return reject(utilities.createError(err.sqlMessage, `Hubo un problema obteniendo los asientos`))
                return resolve(status)
            })
    })
}

function ocuparAsientos({asientos, comprador}){
    return new Promise((resolve, reject) => {
        let a=pool.query(`update asientosCital set status='ocupado',comprador='${comprador==" "? comprador:"anónimo"}' where isActive=1 and id in(${asientos.join(',') })`
            , (err, status) => {
            console.log(a.sql)
                if (err) return reject(utilities.createError(err.sqlMessage, `Hubo un problema ocupando los asientos`))
                return resolve(status)
            })
    })
}
module.exports = {
    getAsientos,
    ocuparAsientos
}