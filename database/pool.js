'use strict'
const mysql = require('mysql')
const constants= require('../constants/passwordsAndKeys')
const pool = mysql.createPool({
    host: '165.227.199.69',
    user: constants.DATABASE_USER ,
    password: constants.DATABASE_PASSWORD,
    database: 'baevi',
    connectionLimit: 50,
    supportBigNumbers: true,
    multipleStatements: true
})

module.exports = {
    pool
}
