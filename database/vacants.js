'use strict'
const utilities = require('../utils')
const pool = require('./pool').pool
const errors = require('../constants/errors')
const Vacants = require('../models/Vacants')

function postVacant({userId, title, description,url}) {
  return new Promise((resolve, reject) => {
    pool.query(`INSERT INTO vacants (publicId,insertedBy,modifiedBy,title,description,url)
      select ?,u.id,u.id,?,?,? from users u where u.publicId=? `, [utilities.generateId(), title, description,url, userId,], (err, status) => {
      if (err) return reject(utilities.createError(err.sqlMessage, `Error creando la vacante`))
      if (!status.insertId) return reject(utilities.createError(errors.INVALID_PARAMS, `Error creando la vacante`))
      return resolve(status.insertId)
    })
  })
}

function patchVacant({userId, params, vacantId}) {
  return new Promise((resolve, reject) => {
    let values = [userId]
    let aux = ""
    Object.keys(params).forEach(k => {
      aux += `, ${k}= ?`
      values.push(params[k])
    })
    values.push(vacantId)
    let a = pool.query(`update vacants set modifiedDate=now(), modifiedBy=(select id from users where publicId=?) ${aux} where publicId=?  `, values, (err, status) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "Error actualizando la vacante"))
      if (status.affectedRows === 0) return reject(utilities.createError(errors.NO_RESUlTS, "Esa vacante no existe"))
      return resolve()
    })
  })
}

function deleteVacant({userId, vacantId}) {
  return new Promise(((resolve, reject) => {
    pool.query(`select id from users where publicId=?`, [userId], (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "Error trayendo al usuario para eliminar la vacante"))
      let idUser = rows[0].id
      pool.query(`update vacants set isActive=0, modifiedBy=?  where publicId=? `, [idUser, vacantId],
        (err, status) => {
          if (err) return reject(utilities.createError(err.sqlMessage, "Error eliminando la vacante"))
          if (status.affectedRows === 0) reject(utilities.createError(errors.NO_RESUlTS, "No existe la vacante"))
          return resolve()
        })
    })
  }))
}

function getVacants(list = []) {
  return new Promise(((resolve, reject) => {
    let aux = ``
    if (list.length) aux = `and publicId in ("${list.join('","')}")`
    pool.query(`select v.publicId, title,description,v.insertedDate,v.modifiedDate,v.url url from vacants v
     where v.isActive=1 ${aux}`, (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "No se pudo traer las vacantes"))
      return resolve(rows.map(r => new Vacants(r)))
    })
  }))
}



module.exports = {
  postVacant,
  deleteVacant,
  patchVacant,
  getVacants,

}