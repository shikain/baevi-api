'use strict'
const utilities = require('../utils')
const pool = require('./pool').pool
const errors = require('../constants/errors')
const Movie = require('../models/Movies')


function postMovie({title, description, url, score, userId, redirectTo}) {
  return new Promise((resolve, reject) => {
    let a = pool.query(`Insert into movies (publicId,insertedBy,modifiedBy,title,description,url,score,redirectTo) 
    select ?,id,id,?,?,?,?,? from users where publicId=?`, [utilities.generateId(), title, description, url, score, redirectTo, userId],
      (err, status) => {
        if (err) return reject(utilities.createError(err.sqlMessage, "No se pudo insertar la pelicula"))
        return resolve(status)
      })
  })
}

function getMovies(list = []) {
  return new Promise(((resolve, reject) => {
    let aux = ``
    if (list.length) aux = `and publicId in ("${list.join('","')}")`

    pool.query(`select publicId, title,description,url,score,insertedDate,modifiedDate,redirectTo from movies where isActive=1`, (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "No se pudo traer las peliculas"))
      return resolve(rows.map(r => new Movie(r)))
    })
  }))
}

function patchMovie({userId, params, movieId}) {
  return new Promise((resolve, reject) => {
    let values = [userId]
    let aux = ""
    Object.keys(params).forEach(k => {
      aux += `, ${k}= ?`
      values.push(params[k])
    })
    values.push(movieId)
    let a = pool.query(`update movies set modifiedDate=now(), modifiedBy=(select id from users where publicId=?) ${aux} where publicId=?  `, values, (err, status) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "Error actualizando la pelicula"))
      if (status.affectedRows === 0) return reject(utilities.createError(errors.NO_RESUlTS, "Esa pelicula no existe"))

      return resolve()
    })
  })
}

function deleteMovie({id, userId}) {
  return new Promise(((resolve, reject) => {
    pool.query(`select id from users where publicId=?`, [userId], (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "Error trayendo al usuario para eliminar la pelicula"))
      let idUser = rows[0].id
      pool.query(`update movies set isActive=0, modifiedBy=?  where publicId=? `, [idUser, id],
        (err, status) => {
          if (err) return reject(utilities.createError(err.sqlMessage, "Error eliminando la pelicula"))
          if (status.affectedRows === 0) reject(utilities.createError(errors.NO_RESUlTS, "No existe la pelicula"))
          return resolve()
        })
    })
  }))
}

module.exports = {
  postMovie,
  getMovies,
  deleteMovie,
  patchMovie
}