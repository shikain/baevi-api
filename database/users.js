'use strict'
const utilities = require('../utils')
const pool = require('./pool').pool
const errors = require('../constants/errors')
const User = require('../models/User')

function login({user,isSocialNetwork}) {
  return new Promise((resolve, reject) => {
    let pass = utilities.encryptValue(user.password)
    const query = `SELECT u.username, u.publicId, u.password, u.fullName, u.email, t.value as role 
                          FROM users u 
                          left join types t on t.id = u.typeId WHERE u.username=? AND u.isActive=1;`
    pool.query(query, [user.username], (err, row) => {
      if (err) {
        return reject(utilities.createError(err.sqlMessage, "There was a problem with the query."))
      }
      if (row.length < 1) {
        return reject(utilities.createError(errors.NO_RESUlTS, "There wasn't matches with that user."))
      }
      let userData = row[0]
      if (pass !== userData.password) {
        return reject(utilities.createError(errors.NO_AUTH, "Wrong password."))
      }
      let newUser = new User(userData)
      return resolve(newUser.json())

    })
  })
}

function create(id, username, password, email, fullName, role, user) {
  return new Promise((resolve, reject) => {
    const query = `INSERT INTO users (publicId, username, password, email, fullName, typeId, insertedBy, modifiedBy)
                      select ?, ?, ?, ?, ?, t.id, u.id, u.id 
                      from types t
                      left join users u on 1=1 where t.value = ? and u.publicId=?`
    pool.query(query, [id, username, password, email, fullName, role, `FIRST_EXAMPLE_USER`], (err, status) => {
      if (err) {
        if (err.sqlMessage.includes('Duplicate entry')) return reject(utilities.createError(err.sqlMessage, "el username ya está registrado"))
        return reject(utilities.createError(err.sqlMessage, "Hubo un problema con los datos del usuario"))
      }
      if (!status.insertId) return reject(utilities.createError(errors.NO_RESUlTS, "Hubo un problema insertando los datos"))
      return resolve(id)
    })
  })
}


function list({id}) {
  return new Promise((resolve, reject) => {
    let query = `select user.username, type.value role, user.publicId, user.fullName, user.email, userInserted.publicId insertedBy, userModified.publicId modifiedBy, user.insertedDate, user.modifiedDate
                        from users user 
                        left join users userInserted on user.insertedBy = userInserted.id 
                        left join users userModified on user.modifiedBy = userModified.id
                        left join types type on user.typeId = type.id
                        where user.isActive=1 `
    let values = []
    if (id) {
      query = query + 'and user.publicId = ?'
      values.push(id)
    }
    pool.query(query, values, (err, rows) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "There wasn't matches with that user."))
      if (!rows.length) return reject(utilities.createError(errors.NO_RESUlTS, "There were no user"))
      return resolve(rows.map(row => new User(row).json()))
    })
  })
}


function remove({id}) {
  return new Promise((resolve, reject) => {
    const query = 'UPDATE users set isActive=0, modifiedDate=current_timestamp where publicId=?;'
    const values = [id]
    pool.query(query, values, (err, status) => { //check if u have permissions to del that user
      if (err) return reject(utilities.createError(err.sqlMessage, "There was a problem deleting user data."))
      if (status.affectedRows === 0) return reject(utilities.createError(errors.NO_RESUlTS, "There was not matches with that user."))
      return resolve(status)
    })
  })
}

function patch({id, params}) {
  return new Promise((resolve, reject) => {
    let keys = Object.keys(params)
    let values = []
    keys.forEach(k => {
      if (k === 'role') {
        k = 'typeId'
        params[k] = params['role'] === 'ADMIN' ? 1 : 2
      }
      values.push(`${k}="${params[k]}"`)
    })
    values.push(`modifiedDate=current_timestamp`)
    const query = `UPDATE users set  ${values.join(',')} where publicId=? and isActive=1`
    pool.query(query, [id], (err, status) => {
      if (err) return reject(utilities.createError(err.sqlMessage, "There was a problem changing user data."))
      if (status.affectedRows === 0) return reject(utilities.createError(errors.NO_RESUlTS, "There was not matches with that user."))
      return resolve(list({id}))
    })
  })
}


module.exports = {
  create,
  login,
  list,
  remove,
  patch,

}
