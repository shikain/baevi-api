'use strict'
const admin = require('firebase-admin')
let serviceAccount = require('./grupo-baevi-firebase-adminsdk-zlap0-3e765a9152')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
})

function sendMessage({token, event, data}) {
  data.event = event
  admin.messaging().send({data, token}).then(res => console.log(res, 1230)).catch(error => console.log(error))
}

let a = {
  "responses": [
    {
    "success": false,
    "error": {"code": "messaging/registration-token-not-registered", "message": "Requested entity was not found."}
  }, {
    "success": false, "error": {
      "code": "messaging/third-party-auth-error", "message": "Auth error from A PNS or Web Push Service"
    }
  }], "successCount": 0, "failureCount": 2
}


function sendMultiMessage({event, data, tokens}) {
  data.event = event
  return admin.messaging().sendMulticast({data, tokens}).then(res => {

    console.log(`Entregado correctamente a: ${res.successCount}`)
    return res
  }).catch(error => {
    console.log(error)
    return error
  })
}

module.exports = {
  admin,
  sendMessage,
  sendMultiMessage
}


