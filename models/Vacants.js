'use strict'
const DataBaseEntity = require('./Entity')
const joi = require('joi')
const utils=require('../utils')

class Vacants extends DataBaseEntity {
  constructor({title,description,url, publicId, createdDate, modifiedDate, insertedBy, modifiedBy, ...others}) {
    super({publicId, createdDate, modifiedDate, insertedBy, modifiedBy})
    this.title = title
    this.description=description
    this.url=url
  }

  json() {
    return JSON.parse(JSON.stringify(this))
  }

  static schema() {
    return joi.object().keys({
      title: joi.string().required().max(100),
      description: joi.string().required().max(1500),
      url: joi.string().required().max(500)
    })
  }


  static validate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, Vacants.schema(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'Datos inválidos, por favor revisarlos.'))
        resolve()
      })
    })
  }
}


module.exports = Vacants