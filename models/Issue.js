'use strict'
const DataBaseEntity = require('./Entity')
const joi = require('joi')
const utils=require('../utils')
const User=require('./User')
class Issue extends DataBaseEntity {
  constructor({solverId,solverUsername,issuerId,issuerUsername,solverName,issuerName,issuerRole,messages,status, publicId, createdDate, modifiedDate, insertedBy, modifiedBy, ...others}) {
    super({publicId, createdDate, modifiedDate, insertedBy, modifiedBy})
    this.solver =new User({username:solverUsername,publicId:solverId,fullName:solverName})
    this.issuer =new User({username:issuerUsername,publicId:issuerId,fullName:issuerName,role:issuerRole})
    this.messages= []
    this.status=status

  }

  json() {
    return JSON.parse(JSON.stringify(this))
  }
  setMessages(messages){
    this.messages=messages
  }

  static schema() {
    return joi.object().keys({
      name: joi.string().required(),
      type: joi.string().required(),

    })
  }


  static validate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, Issue.schema(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'Datos inválidos, por favor revisarlos.'))
        resolve()
      })
    })
  }
}

module.exports = Issue
