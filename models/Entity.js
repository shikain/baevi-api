'use strict'
const CustomDate = require('../models/CustomDate')

class Entity {
  constructor({id, publicId, insertedDate, modifiedDate, isActive, insertedBy, modifiedBy, ...others}) {
    //if (id) this.id = id
    if (publicId) this.id = publicId.substr(0, 100)
    if (publicId) this.tinyId = publicId.substr(0, 10)
    if (isActive) this.isActive = isNaN(Number(isActive)) ? `-1` : Number(isActive)
    if (insertedDate) {
      this.insertedDate = new CustomDate()
      this.insertedDate.setByString({mysqlString: insertedDate})
    }
    if (modifiedDate) {
      this.modifiedDate = new CustomDate()
      this.modifiedDate.setByString({mysqlString: modifiedDate})
    }
    this.insertedBy = insertedBy
    this.modifiedBy = modifiedBy
  }
}

module.exports = Entity
