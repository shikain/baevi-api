'use strict'
const DataBaseEntity = require('./Entity')
const joi = require('joi')
const utils=require('../utils')

class Promotion extends DataBaseEntity {
  constructor({name,logo,description,image1,image2,image3,image4,facebook,instagram,twitter,site,publicId, createdDate, modifiedDate, insertedBy, modifiedBy,address,latitude,longitude,phone,textAddress, ...others}) {
    super({publicId, createdDate, modifiedDate, insertedBy, modifiedBy})
    this.name=name
    this.logo=logo
    this.description=description
    this.image1=image1
    this.image2=image2
    this.image3=image3
    this.image4=image4
    this.facebook=facebook
    this.twitter=twitter
    this.instagram=instagram
    this.site=site
    this.address=address
    this.latitude=latitude
    this.longitude=longitude
    this.phone=phone
    this.textAddress=textAddress
  }

  json() {
    return JSON.parse(JSON.stringify(this))
  }

  static schema() {
    return joi.object().keys({
      name: joi.string().required().max(100),
      logo: joi.string().required().max(300),
      description: joi.string().required().max(500),
      image1: joi.string().required().max(300),
      image2: joi.string().required().max(300),
      image3: joi.string().required().max(300),
      image4: joi.string().required().max(300),
      facebook: joi.string().required().max(100),
      twitter: joi.string().required().max(100),
      instagram: joi.string().required().max(100),
      site: joi.string().required().max(100)
    })
  }

  static validate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, Promotion.schema(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'Datos inválidos, por favor revisarlos.'))
        resolve()
      })
    })
  }

}

module.exports = Promotion
