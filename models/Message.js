'use strict'
const DataBaseEntity = require('./Entity')
const joi = require('joi')
const utils=require('../utils')

class Message extends DataBaseEntity {
  constructor({text,issueId, publicId, insertedDate, modifiedDate, insertedBy, modifiedBy, ...others}) {
    super({publicId, insertedDate, modifiedDate, insertedBy, modifiedBy})
    this.text = text
    this.issue=issueId

  }

  json() {
    return JSON.parse(JSON.stringify(this))
  }

  static schema() {
    return joi.object().keys({
      text: joi.string().max(500).required(),
      issue: joi.string().required(),

    })
  }


  static validate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, Message.schema(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'Datos inválidos, por favor revisarlos.'))
        resolve()
      })
    })
  }
}

module.exports = Message
