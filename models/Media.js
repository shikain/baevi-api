'use strict'
const DataBaseEntity = require('./Entity')
const joi = require('joi')
const utils=require('../utils')

class Media extends DataBaseEntity {
  constructor({name, type,url, publicId, createdDate, modifiedDate, insertedBy, modifiedBy, ...others}) {
    super({publicId, createdDate, modifiedDate, insertedBy, modifiedBy})
    this.type = type
    this.url = url

  }

  json() {
    return JSON.parse(JSON.stringify(this))
  }

  static schema() {
    return joi.object().keys({
      type: joi.string().required(),
      url: joi.string().required().max(500)
    })
  }


  static validate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, Media.schema(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'Datos inválidos, por favor revisarlos.'))
        resolve()
      })
    })
  }
}

module.exports = Media
