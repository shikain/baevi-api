'use strict'
const joi = require('joi')
const moment = require('moment')
const utils = require('../utils')

class CustomDate {
  constructor() {
  }

  setByDate({day, month, year, hour = 1, minute = 0, second = 0, miliseconds}) {
    this.day = day
    this.month = month
    this.year = year
    this.hour = hour
    this.minute = minute
    this.second = second
    this.moment = moment([year, month, day, hour, minute, second])
    this.setByString({momento: this.moment})
  }

  setByString({mysqlString, momento}) {
    this.moment = moment(mysqlString) || momento
    this.moment.locale('es')
    this.complete = this.moment.format('D MMMM YYYY - hh:mm:ss a')
    this.short = this.moment.format('D MMM YYYY - hh:mm:ss a')
    this.completeNoTime = this.moment.format('D MMMM YYYY')
    this.shortNoTime = this.moment.format('D MMM YYYY')
    this.completeNumeric = this.moment.format('D/MM/YYYY - hh:mm:ss a')
    this.shortNumeric = this.moment.format('D/MM/YY - hh:mm:ss a')
    this.completeNumericNoTime = this.moment.format('D/MM/YYYY')
    this.shortNumericNoTime = this.moment.format('D/MM/YY')
    this.day = this.moment.date()
    this.month = this.moment.month()+1
    this.year = this.moment.year()
    this.hour = this.moment.hours()
    this.minute = this.moment.minutes()
    this.second = this.moment.seconds()
  }

  json() {
    return JSON.parse(JSON.stringify(this))
  }

  static schema() {
    return joi.object().keys({
      day: joi.number().integer().min(1).max(31).required(),
      month: joi.number().integer().min(1).max(12).required(),
      year: joi.number().integer().min(1900).max(2100).required(),
      hours: joi.number().integer().min(9).max(23).required(),
      minutes: joi.number().integer().min(0).max(59).required()
    })
  }

  validate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, CustomDate.schema(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'date invalido, verifica los datos.'))
        resolve()
      })
    })
  }
}

module.exports = CustomDate
