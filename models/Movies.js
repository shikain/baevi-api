'use strict'
const DataBaseEntity = require('./Entity')
const joi = require('joi')
const utils=require('../utils')

class Movies extends DataBaseEntity {
  constructor({title,description,url,score,redirectTo, publicId, createdDate, modifiedDate, insertedBy, modifiedBy, ...others}) {
    super({publicId, createdDate, modifiedDate, insertedBy, modifiedBy})
    this.title = title
    this.description=description
    this.url=url
    this.score=score
    this.redirectTo=redirectTo
  }

  json() {
    return JSON.parse(JSON.stringify(this))
  }

  static schema() {
    return joi.object().keys({
      title: joi.string().required().max(100),
      description: joi.string().required().max(1000),
      url: joi.string().required().max(300),
      score: joi.number().required().min(0).max(10),
      redirectTo:joi.string().required().max(300)
    })
  }


  static validate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, Movies.schema(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'Datos inválidos, por favor revisarlos.'))
        resolve()
      })
    })
  }
}

module.exports = Movies
