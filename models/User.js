'use strict'
const DataBaseEntity = require('./Entity')
const joi = require('joi')
const utils=require('../utils')

class User extends DataBaseEntity {
  constructor({fullName, email, password, role, username, publicId, createdDate, modifiedDate, insertedBy, modifiedBy, ...others}) {
    super({publicId, createdDate, modifiedDate, insertedBy, modifiedBy})
    this.fullName = fullName
    this.email = email
    this.password = password
    this.role = role
    this.username = username
  }

  json() {
    return JSON.parse(JSON.stringify(this))
  }

  static schema() {
    return joi.object().keys({
      fullName: joi.string().required(),
      email: joi.string().required(),
      password: joi.string().min(6).max(50).required(),
      role: joi.string().valid("Invitado", "Administrador",).required(),
      username: joi.string().min(2).max(100).required(),
    })
  }

  static schemaLogin() {
    return joi.object().keys({
      password: joi.string().min(6).max(50).required(),
      username: joi.string().min(2).max(100).required(),
    })
  }

  static validate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, User.schema(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'Datos inválidos, por favor revisarlos.'))
        resolve()
      })
    })
  }

  static loginValidate(obj) {
    return new Promise((resolve, reject) => {
      joi.validate(obj, User.schemaLogin(), (error) => {
        if (error) return reject(utils.createError(error.details.map(detail => detail.message).join(' and '), 'Datos inválidos para iniciar sesión.'))
        resolve()
      })
    })
  }
}

module.exports = User
