'use strict'
const express = require('express')
const path = require('path')
const logger = require('morgan')
const helmet = require('helmet')
const config = require('./config')
const cors = require('cors')
const bodyParser = require('body-parser')
const utils = require('./utils')


const usersRouter = require('./routes/users')
const mediaRouter = require('./routes/medias')
const moviesRouter =require('./routes/movies')
const vacantsRouter =require('./routes/vacants')
const issuesRouter = require('./routes/issues')
const promotionsRouter = require('./routes/promotions')
const citalRouter = require('./routes/cital')
const firebaseRouter = require('./routes/firebase')

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(helmet())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(express.static(path.join(__dirname, 'public')))



app.use('/api/users', usersRouter)
app.use('/api/medias', mediaRouter)
app.use('/api/movies',moviesRouter)
app.use('/api/vacants',vacantsRouter)
app.use('/api/promotions',promotionsRouter)
app.use('/api/issues', issuesRouter)
app.use('/api/cital', citalRouter)
app.use('/api/firebase', firebaseRouter)

app.use('/dist', express.static(__dirname + '/dist'))
app.use('/.well-known', express.static(__dirname + '/.well-known'))
app.use('/plantilla', express.static(__dirname + '/plantilla'))
app.use('/firebase-messaging-sw.js', express.static(__dirname + '/plantilla'))

app.get(["/", "/Contact", "/About", "/Login", "/Media", "/BolsaDeTrabajo", "/Promotions/:name"], (req, res)=> {
  res.sendFile(path.dirname(require.main.filename) + '/index.html')
})
app.get('/firebase-messaging-sw.js', (req, res)=> {
  res.sendFile(path.dirname(require.main.filename) + '/firebase-messaging-sw.js')
})


let server = app.listen(config.port, function (err) {
  if (err) return console.log('Hubo un error :(')
  console.log(`Listening at ${config.port}`)
  console.log(utils.encryptValue("gatico"))

})

