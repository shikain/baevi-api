'use strict'
const DATABASE_PASSWORD = process.env.BAEVIPASSWORD
const DATABASE_USER = process.env.BAEVIUSER
module.exports = {
  DATABASE_PASSWORD,
  DATABASE_USER
}
