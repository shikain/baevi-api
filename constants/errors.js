const NO_AUTH =  'You do not have authorization'
const INVALID_TOKEN = 'Your token is invalid'
const WRONG_QUERY= 'There was a problem executing the query'
const NO_RESUlTS = 'There was not matches with the query'
const INVALID_PARAMS  = 'Params are not valid.'
const UPLOADED_FIlE = 'Error uploading the file'
module.exports={
    NO_AUTH,
    INVALID_TOKEN,
    WRONG_QUERY,
    NO_RESUlTS,
    INVALID_PARAMS,
    UPLOADED_FIlE
}

