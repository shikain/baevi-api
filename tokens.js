'use strict'
const jwt = require('jsonwebtoken')
const extractor = require('token-extractor')
const moment = require('moment')
const utils = require('./utils')
const ce = require('./constants/errors')
const security = false
const secret = process.env.TOKENS_KEYS || 'SYSTEM_FYPR'

function create(payload, options) {
  return new Promise((resolve, reject) => {
    payload.expirationDate = process.env.NODE_ENV === 'DEVELOP' ? moment().add(200, 'days').unix() : moment().add(200, 'days').unix()
    jwt.sign(payload, secret, options, (error, token) => {
      if (error) return reject(error)
      payload.token = token
      delete payload['expirationDate']
      console.log('token generated :D')
      return resolve(payload)
    })
  })
}

function createToEmail(payload, options) {
  return new Promise((resolve, reject) => {
    payload.expirationDate = moment().add(3, 'days').unix()
    jwt.sign(payload, secret, options, (error, token) => {
      if (error) return reject(error)
      payload.token = token
      delete payload['expirationDate']
      console.log('token generated :D')
      return resolve(payload)
    })
  })
}

function verify(token, options) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secret, options, (error, payload) => {
      if (error) return reject(error)
      return resolve(payload)
    })
  })
}

function obtain(req) {
  return new Promise((resolve, reject) => {
    extractor(req, (error, token) => {
      if (error) return reject(error)
      resolve(token)
    })
  })
}

function validate(req, res, next) {
  if (!req.headers.authorization) return res.status(403).send(utils.createError(ce.NO_AUTH))
  obtain(req)
    .then(token => verify(token))
    .then(payload => {
      if (payload.expirationDate < moment().unix()) return res.status(401).send(utils.createError(ce.INVALID_TOKEN))
      req.user = payload
      next()
    }).catch(error => {
    return security || res.status(401).send(utils.createError(ce.INVALID_TOKEN))
  })
}

module.exports = {
  create,
  validate,
  createToEmail,
  verify
}
