'use strict'
const express = require('express')
const router = express.Router()
const tokens = require('../tokens')
const utils = require('../utils')
const Multer= require('multer')
const mediaDb=require('../database/medias')
const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 30 * 1024 * 1024
  },

})


router.patch('/playList',tokens.validate, (req, res) => {
  const body=req.body
  const user=req.user
  return mediaDb.updatePlaylist({userId:user.id,url:body.url}).then(r=>{
    return res.status(204).send()
  }).catch(err=>{
    console.log(err)
    return res.status(500).send()
  })
})
router.get('/playList', (req, res) => {
  return mediaDb.getPlaylist().then(response=>{
    return res.status(200).send(response.url)
  }).catch(err=>{
    console.log(err)
    return res.status(500).send()
  })
})

router.get('/', (req, res) => {
  return mediaDb.getMedia({}).then(response=>{
    return res.status(200).send(response)
  }).catch(err=>{
    console.log(err)
    return res.status(500).send()
  })
})
router.post('/',tokens.validate, (req, res) => {
  let body=req.body
  let user=req.user
  return mediaDb.postMedia({type:body.type, userId:user.id,url:body.url}).then(response=>{
    return res.status(204).send()
  }).catch(err=>{
    console.log(err)
    return res.status(500).send()
  })
})
router.delete('/:mediaId',tokens.validate, (req, res) => {
  let params=req.params
  let user=req.user
  return mediaDb.deleteMedia({mediaId:params.mediaId,userId:user.id}).then(response=>{
    return res.status(204).send()
  }).catch(err=>{
    console.log(err)
    return res.status(500).send()
  })
})







module.exports=router