'use strict'
const express = require('express')
const router = express.Router()
const tokens = require('../tokens')
const utils = require('../utils')
const vacantsDb = require('../database/vacants')
const Vacants = require('../models/Vacants')
const axios = require('axios')

router.post('/', tokens.validate, (req, res) => {
  let body = req.body
  let user = req.user
  return Vacants.validate(body).then(() => {
    return vacantsDb.postVacant({userId: user.id, title: body.title, description: body.description, url: body.url})
  }).then(() => {
    return res.status(204).send()
  }).catch(err => {
    console.log(err)
    return res.status(500).send()
  })
})

router.get('/', (req, res) => {
  return vacantsDb.getVacants().then(vacants => {
    return res.status(200).send(vacants)
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})

router.get('/:vacantId', (req, res) => {
  let params = req.params
  return vacantsDb.getVacants([params.vacantId]).then(vacant => {
    return res.status(200).send(vacant)
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})
router.delete('/:vacantId', tokens.validate, (req, res) => {
  let params = req.params
  let user = req.user
  return vacantsDb.deleteVacant({userId: user.id, vacantId: params.vacantId}).then(() => {
    return res.status(204).send()
  }).catch(err => {
    console.log(err)
    return res.status(500).send()
  })
})

router.patch('/:vacantId', tokens.validate, (req, res) => {
  let params = req.params
  let body = req.body
  let user = req.user
  let changedParams = {}
  if (body.title) changedParams.title = body.title
  if (body.url) changedParams.url = body.url
  if (body.description) changedParams.description = body.description
  return vacantsDb.patchVacant({params: changedParams, userId: user.id, vacantId: params.vacantId}).then(() => {
    return res.status(204).send()
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})


router.post('/sendMail', tokens.validate, (req, res) => {
  let url = 'http://localhost:8091/api/baeviMail'
  let body = req.body

  axios.post(url, {
    sender: 'contacto@fyf.mx',
    receiver: ``,
    apiKey: "FcH<4d}ce3T,);EY",
    message: `<h2>Hola Grupo Baevi, has recibido una nueva solicitud de empleo.</h2>    
    <h3>Mi nombre es: ${body.name}. Mi número es: ${body.number}. Mi correo es:${body.address}  </h3>
    <br>
${body.text}`,

    subject: `Se aplicado para una nueva vacante de ${body.title}`
  }).then(r => {
    res.status(204).send()
  }).catch(err => {
    console.log(err)
  })

})


module.exports = router