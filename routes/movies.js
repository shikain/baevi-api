'use strict'
const express = require('express')
const router = express.Router()
const tokens = require('../tokens')
const utils = require('../utils')
const Multer= require('multer')
const moviesDb=require('../database/movies')
const Movies =require('../models/Movies')

router.post('/',tokens.validate,(req,res)=>{
  let body=req.body
  let user=req.user
  return Movies.validate(body).then(()=>{
    return moviesDb.postMovie({userId:user.id,score:body.score,title:body.title,url:body.url,description:body.description,redirectTo:body.redirectTo})
  }).then(()=>{
    return res.status(204).send()
  }).catch(err=>{
    console.log(err)
    return res.status(500).send()
  })
})

router.get('/',(req,res)=>{
  return moviesDb.getMovies().then(movies=>{
    return res.status(200).send(movies)
  }).catch(err=>{
    console.log(err)
    return res.status(500).send(err)
  })
})
router.get('/:movieId',(req,res)=>{
  let params=req.params
  return moviesDb.getMovies([params.movieId]).then(movies=>{
    return res.status(200).send(movies)
  }).catch(err=>{
    console.log(err)
    return res.status(500).send(err)
  })
})
router.delete('/:movieId',tokens.validate,(req,res)=>{
  let params=req.params
  let user=req.user
  return moviesDb.deleteMovie({userId:user.id,id:params.movieId}).then(()=>{
    return res.status(204).send()
  }).catch(err=>{
    console.log(err)
    return res.status(500).send()
  })
})

router.patch('/:movieId',tokens.validate,(req,res)=>{
  let params=req.params
  let body=req.body
  let user=req.user
  let changedParams={}
  if(body.title) changedParams.title=body.title
  if(body.url) changedParams.url=body.url
  if(body.description) changedParams.description=body.description
  if(body.score)changedParams.score=body.score
  if(body.redirectTo)changedParams.redirectTo=body.redirectTo
  return moviesDb.patchMovie({params:changedParams,userId:user.id,movieId:params.movieId}).then(()=>{
    return res.status(204).send()
  }).catch(err=>{
    console.log(err)
    return res.status(500).send(err)
  })
})




module.exports=router