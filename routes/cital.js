'use strict'
const express = require('express')
const router = express.Router()
const tokens = require('../tokens')
const citalDb = require('../database/cital')
const axios=require('axios')
// SDK de Mercado Pago
const mercadopago = require("mercadopago")
mercadopago.configure({
    access_token: "APP_USR-6623812045942093-011918-f2d132bdeee46ff7eda96bfbe9942a72-619375218",
})

router.get('/',(req,res)=> {

    return citalDb.getAsientos().then(asientos=>{
        let table=`<style>

.btn_ocupado{
background-color: red;
 pointer-events: none;
 opacity: .8;
 width: 57px;
     border-radius: 100%;
}
.btn_lateral.btn_pendiente{
background-color: #269bd1 !important;
opacity: .8;
width: 57px;
border-radius: 100%;
}
.btn_disponible.btn_pendiente{
background-color: #269bd1 !important;
opacity: .8;
width: 57px;
border-radius: 100%;
}

.btn_vip.btn_pendiente{
background-color: #269bd1 !important;
opacity: .8;
width: 57px;
border-radius: 100%;
}
.btn_lateral{
background-color: pink;
opacity: .8;
width: 57px;
border-radius: 100%;
}
.btn_disponible{
opacity: .8;
width: 57px;
border-radius: 100%;
}
.btn_vip{
opacity: .8;
width: 57px;
border-radius: 100%;
background-color: black;
}
.btn_instructores{
background-color: orange;
pointer-events: none;
opacity: .8;
width: 57px;
border-radius: 100%;
}
td{
border: 0px!important;
padding: 0px !important;
margin: 0px !important;
}
table{

border: 0px !important;
padding: 0px !important;
margin: 0px !important;

}
tr{
padding: 0px !important;
margin: 0px !important;
}
</style>
<input type="hidden" value="" id="hiddenset"/>
<script>
const mySet = new Set();
let total=0
function agregar(id){
    
let price=4000
if(id>150) price=3700
if(id>150 && ((id%25>=0 && id%25<=5) ||  (id%25>20 && id%25<=25))) price=2600

let totalSpan=document.getElementById("summary-total")
let btn=document.getElementById("btn_"+id)
if(!btn.classList.contains("btn_pendiente")) {
    btn.classList.add("btn_pendiente")
    mySet.add(id)
    
    total+=price
    totalSpan.innerHTML = "$ " + total;
}

else if(btn.classList.contains("btn_pendiente")) {
      btn.classList.remove("btn_pendiente")
       mySet.delete(id)
       total-=price
       totalSpan.innerHTML = "$ " + total;
      let asientos=''
      mySet.forEach((value) => {
  asientos+=" "+value+" "
})
}

    let hidden=document.getElementById('hiddenset')
    hidden.value=""
        for (const item of mySet) {
        hidden.value=hidden.value+','+item
    }
        hidden.value=hidden.value.substring(1)
}


</script>
<table>`
let id=0
        for (let i=0;i<29;i++){
            table+='<tr>'
            for (let o=0;o<25;o++){

                table+=`<td style=""><button value="${asientos[id].publicId}" class="btn_${asientos[id].status}" onclick="agregar('${id+1}')"  id="btn_${id+1}" >${id+1}</button></td>`
                if((o+1)%5==0)table+=`<td>___</td>`
                id++
            }
            table+='</tr>'
        }
        return res.status(200).send(table)
    }).catch(err=>{
        console.log(err)
        return res.status(500).send(err)
    })
})
router.post("/create_preference", (req, res) => {
    let asientos=req.body.quantity
    let cupon=req.body.cupon
    console.log("el cupon", asientos,cupon,req.body)
    let objt=asientos.split(',')
    console.log(objt)
    let precioVip=4000,precioCentral=3700,precioGeneral=2600
if(cupon==='DIA CITAL' ||cupon==='CHINOCELLS' ||cupon==='OPTIMUS' ||cupon==='LA CASA DEL CENTRO DE CARGA' ||cupon==='CELLBOUTIQUE' ||cupon==='PANDA TEAM' ||cupon==='CELLZONE' ||cupon==='TRAINING CENTER' ||cupon==='MR CELL' ||cupon==='G TECNOLOGIA' ||cupon==='MARKETECH' ||cupon==='TOOL ROOM' ||cupon==='MC ACADEMY' ||cupon==='MR FIX' ){
    precioCentral=precioCentral-500
    }
if(cupon==='WILTECH'){
    precioCentral=precioCentral-700
    }
if(cupon==='CITEB' || cupon=='MOVIMIENTO FEMENINO'){
    precioCentral=precioCentral-1100
    }
if(cupon==='fasdf24xd3qwrqw3xq234rdx43r23x423rx23rq523'){
    precioCentral=3
    precioVip=3
    precioGeneral=3
    }
return citalDb.getAsientos(objt).then(lugares=>{
    let lugaresVip=[]
    let lugaresGeneral=[]
    let lugaresCentral=[]
    console.log(lugares)
    lugares.forEach(r=>{
        if(r.status==='vip')lugaresVip.push(r)
        if(r.status==='lateral')lugaresGeneral.push(r)
        if(r.status==='disponible')lugaresCentral.push(r)
    })
    let titleVip='Asientos cital vip'
    lugaresVip.forEach(r=>{
        titleVip+=` no. ${r.id}`
    })
    let titleCentral='Asientos cital central'
    lugaresCentral.forEach(r=>{
        titleCentral+=` no. ${r.id}`
    })
    let titleGeneral='Asientos cital general'
    lugaresGeneral.forEach(r=>{
        titleGeneral+=` no. ${r.id}`
    })
    let items=[]
    if(lugaresVip.length)items.push({
        title: `${titleVip}`,
        unit_price: precioVip,
        quantity: Number(lugaresVip.length),
    })
    if(lugaresCentral.length) items.push({
        title: `${titleCentral} `,
        unit_price: precioCentral,
        quantity: Number(lugaresCentral.length),
    })
    if(lugaresGeneral.length)items.push({
        title:`${titleGeneral} `,
        unit_price: precioGeneral,
        quantity: Number(lugaresGeneral.length),
    })
    console.log(items)
    let preference = {
        items: items,
        back_urls: {
            "success": "https://cital.mx/seleccion-de-asiento",
            "failure": "https://cital.mx/seleccion-de-asiento",
            "pending": "https://cital.mx/seleccion-de-asiento"
        },
        auto_return: "approved",
    };

    return mercadopago.preferences.create(preference)
}).then(function (response) {
            res.json({
                id: response.body.id
            });
        }).catch(function (error) {
        console.log(error);
    });
});
router.post("/pagos", (req, res) => {
    console.log(req.body)

    return axios.get( `https://api.mercadopago.com/v1/payments/${req.body.data.id}`,{ headers: {"Authorization" : `Bearer TEST-4105558237072841-050910-126d407a11fcce522f1821a6e085d733-292206511`}}).
    then(r=>{
        let asientosArr=r.data.additional_info.items
        let asientos=[]
        asientosArr.forEach(r=>{

        r.title.split(' no. ').slice(1).forEach(l=>{
            console.log(l)
            asientos.push(l)
        })
        })
        console.log(asientos,"estos son los asientos")
        citalDb.ocuparAsientos({asientos: asientos,comprador:`${r.data.payer.first_name} ${r.data.payer.last_name}`
    })
        return res.status(200).send()
    }).catch(e=>{
        console.log(e)
        return res.status(500).send()
    })
});


module.exports=router
