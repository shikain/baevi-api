const express = require('express')
const router = express.Router()
const userDB = require('../database/users')
const tokens = require('../tokens')
const User = require('../models/User')
const util = require('../utils')
const errors = require('../constants/errors')

router.post('/token', (req, res) => {
  const data = req.body
  User.loginValidate(data).then(() => {
    return userDB.login({user:data})
  }).then(userData => {
    return tokens.create(userData)
  }).then(tokenedUser => {
    delete tokenedUser.password
    res.status(200).send(tokenedUser)
  }).catch(error => {
    console.log(error)
    return res.status(500).send(error)
  })
})

router.post('/',  (req, res) => {
  const data = req.body
  const user = req.user
  //if (user.role !== "ADMIN") return res.status(401).send(util.createError(errors.INVALID_TOKEN, "You cant use this route."))
  User.validate(data).then(() => {
    let newUser = new User(data)
    newUser.id = util.generateId()
    return userDB.create(newUser.id, newUser.username, util.encryptValue(newUser.password), newUser.email, newUser.fullName, newUser.role, user)
  }).then(id => {
    if (data.role === "STORER") return userDB.relationUserCarrier({
      id: util.generateId(),
      carrier: data.carrier,
      userId: id,
      user
    })
    else if (data.role === "STORER_MANAGER" || data.role === "ADMIN") return userDB.relationUserCarrier({
      userId: id,
      user
    })
    else return Promise.resolve({id})
  }).then(userId => {
    return res.status(200).send(userId)
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})

router.get('/', tokens.validate, (req, res) => {
  const user = req.user
  userDB.list({}).then(users => {
    return res.status(200).send({users})
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})

router.get('/:userId', tokens.validate, (req, res) => {
  const params = req.params
  userDB.list({id: params.userId}).then(user => {
    return res.status(200).send({user: user[0]})
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})

router.patch('/:userId', tokens.validate, (req, res) => {
  const params = req.params
  const user = req.user
  const data = req.body
  if (!data.username && !data.email && !data.fullName && !data.role) return res.status(500).send(util.createError(errors.INVALID_PARAMS, "Request have to contain at least one paramter"))
  // if (user.role !== "ADMIN") return res.status(401).send(util.createError(errors.INVALID_TOKEN, "You cant use this route"))
  let changedParams = {}
  if (data.username) changedParams.username = data.username
  if (data.email) changedParams.email = data.email
  if (data.fullName) changedParams.fullName = data.fullName
  if (data.role) changedParams.role = data.role
  return userDB.patch({id: params.userId, params: changedParams}).then(user => {
    return res.status(200).send({user: user[0]})
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})

router.delete('/:userId', tokens.validate, (req, res) => {
  const params = req.params
  const user = req.user
  if (user.role !== "ADMIN") return res.status(401).send(util.createError(errors.INVALID_TOKEN, "You cant use this route")) //ask for bussines rule to delete users
  return userDB.remove({id: params.userId}).then(() => {
    return res.status(204).send()
  }).catch(err => {
    return res.status(500).send(err)
  })

})



module.exports = router
