'use strict'
const express = require('express')
const router = express.Router()
const tokens = require('../tokens')
const promotions = require('../database/promotions')


router.get('/',(req,res)=> {

return promotions.list().then(promotions=>{
  return res.status(200).send(promotions)
}).catch(err=>{
  console.log(err)
  return res.status(500).send(err)
})
})
router.get('/fixers',(req,res)=> {

return promotions.listFixers().then(promotions=>{
  return res.status(200).send(promotions)
}).catch(err=>{
  console.log(err)
  return res.status(500).send(err)
})
})

module.exports=router