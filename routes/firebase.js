const express = require('express')
const joi = require('joi')
const utils = require("../utils")
const tokens = require('../tokens')
const firebaseDB = require('../database/firebase')
const router = express.Router()

router.post('/', tokens.validate, (req, res) => {
  if (!req.body.token) return res.status(500).send(utils.createError(undefined,'Firebase token is not provided'))
  firebaseDB.create({userPublicId:req.user.id, tokenId: req.body.token}).then(()=>{
    res.status(204).send()
  }).catch(error => {
    console.log(error)
    res.status(500).send(error)
  })
})

module.exports = router
