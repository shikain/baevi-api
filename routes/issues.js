const express = require('express')
const router = express.Router()
const issuesDB = require('../database/issues')
const firebaseDB = require('../database/firebase')
const tokens = require('../tokens')
const User = require('../models/User')
const util = require('../utils')
const errors = require('../constants/errors')
const admin = require('firebase-admin')
const firebase = require('../firebase')
const serviceAccount = require("../grupo-baevi-firebase-adminsdk-zlap0-3e765a9152.json")

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL: "https://grupo-baevi.firebaseio.com"
// });

router.post('/', tokens.validate, (req, res) => {
  const user = req.user
  let issue
  issuesDB.create({issuer: user.id}).then(id => {
    issuesDB.getIssue({issueId: id}).then(resp => {
      issue = resp
      return firebaseDB.list({typesOfUser: [1]})
    }).then(tokens => {
      console.log(tokens)
      const notificationMessage = `Se creo un nuevo issue`
      firebase.sendMultiMessage({
        tokens,
        data: {
          message: notificationMessage,
          data: JSON.stringify(issue)
        },
        event: 'newIssue'
      })
    })
    return res.status(200).send(id)
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })

})
router.post('/:issueId/messages', tokens.validate, (req, res) => {
  const user = req.user
  const params = req.params
  const body = req.body
  issuesDB.postMessage({messagerId: user.id, issueId: params.issueId, text: body.text}).then(id => {
    issuesDB.getIssue({issueId: params.issueId}).then(issue => {
      console.log(issue)
      if (issue[0].solver.id) {
        return firebaseDB.list({userIds: [issue[0].issuer.id, issue[0].solver.id]})
      }
      else return firebaseDB.list({typesOfUser: [1]})
    }).then(tokens => {
      console.log(tokens)
      let date = new Date()
      const data = {
        sender: user.id,
        senderUsername: user.fullName,
        text: body.text,
        issueId: params.issueId,
        date: `${date.getHours()}:${date.getMinutes()}`
      }
      const notificationMessage = `Tienes un nuevo mensaje`
      firebase.sendMultiMessage({
        tokens,
        data: {
          message: notificationMessage,
          data: JSON.stringify(data)
        },
        event: 'newMessage'
      })
    })
    return res.status(204).send()
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })

})
router.get('/:issueId/messages', tokens.validate, (req, res) => {
  const query = req.query
  const params = req.params
  issuesDB.listMessages({issueId: params.issueId, offset: query.offset, limit: query.limit}).then(id => {
    return res.status(200).send(id)
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})

router.get('/:issueId', tokens.validate, (req, res) => {
  const params = req.params
  let issueObj
  issuesDB.getIssue({issueId: params.issueId}).then(issue => {
    issueObj = issue[0]
    return issuesDB.listMessages({issueId: params.issueId, limit: 10, offset: 10})
  }).then(messages => {
    issueObj.setMessages(messages)
    return res.status(200).send(issueObj.json())
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
})

router.post('/:issueId/take', tokens.validate, (req, res) => {
  const user = req.user
  let params = req.params
  let issueData
  issuesDB.takeIssue({userId: user.id, issueId: params.issueId}).then(() => {
    issuesDB.getIssue({issueId: params.issueId}).then(issue => {
      issueData = issue
      return Promise.all([firebaseDB.list({userIds: [issue[0].issuer.id]}), firebaseDB.list({typesOfUser: [1]}), ])
    }).then(tokens => {
      const dataIssuer = {
        issueId: params.issueId,
        solverName: issueData[0].solver.fullName
      }
      const notificationMessageIssuer = `Tu conversación ha sido asingada`
      firebase.sendMultiMessage({
        tokens: tokens[0],
        data: {
          message: notificationMessageIssuer,
          data: JSON.stringify(dataIssuer)
        },
        event: 'asaingIssue'
      })

      console.log(tokens[1])
      const notificationMessageAdmin = `Converzacion asignada`
      firebase.sendMultiMessage({
        tokens: tokens[1],
        data: {
          message: notificationMessageAdmin,
          data: JSON.stringify(issueData)
        },
        event: 'asaingIssueAdmin'
      })
    })
    return res.status(204).send()
  }).catch(err => {
      console.log(err)
      return res.status(500).send(err)
    })
  })

  router.post('/:issueId/finish', tokens.validate, (req, res) => {
    const user = req.user
    let params = req.params
    issuesDB.finishIssue({issueId: params.issueId, userId: user.id}).then(() => {
      return issuesDB.getIssue({issueId: params.issueId})
    }).then(issue => {
      return firebaseDB.list({userIds: [issue[0].issuer.id]})
    }).then(tokens => {
      let date = new Date()
      const data = {
        sender: user.id,
        issueId: params.issueId,
        date: `${date.getHours()}:${date.getMinutes()}`
      }
      const notificationMessage = `La conversación ha sido finalizada`
      firebase.sendMultiMessage({
        tokens,
        data: {
          message: notificationMessage,
          data: JSON.stringify(data)
        },
          event: 'finishIssue'
      })
    }).then(() => {
      return res.status(204).send()
    }).catch(err => {
      console.log(err)
      return res.status(500).send(err)
    })
  })


  router.get('/', tokens.validate, (req, res) => {
    const params = req.params
    const user = req.user
    let userId = user.role === `Invitado` ? user.id : null
    issuesDB.getIssue({issueId: params.issueId, userId}).then(issues => {
      return res.status(200).send(issues)
    }).catch(err => {
      console.log(err)
      return res.status(500).send(err)
    })
  })


  module.exports = router